package bluetooth;

import android.os.AsyncTask;

import observer.Observable;


public class MessageHandlerTask extends AsyncTask<String, Void, Void> {
    private String msg;

    @Override
    protected Void doInBackground(String... params) {
        msg = params[0];
        return null;
    }

    /*
     * This method is always executed by the main thread
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Observable.get().notifyAll(msg);
    }
}
