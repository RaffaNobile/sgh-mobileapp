package bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Set;
import java.util.UUID;

public class ConnectionInit {

    private static final String BT_SERVER_NAME = "SGHBT";
    private static final String APP_UUID = "00001101-0000-1000-8000-00805F9B34FB";

    private BluetoothAdapter btAdapter;
    private BluetoothDevice btServer;

    public void init() {

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(BT_SERVER_NAME)) {
                    btServer = device;
                }
            }
        }
        UUID uuid = UUID.fromString(APP_UUID);
        new ConnectionTask(btServer, uuid).execute();

    }

}
