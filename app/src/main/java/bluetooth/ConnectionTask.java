package bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.UUID;

public class ConnectionTask extends AsyncTask<Void, Void, Void> {

    private BluetoothSocket btSocket = null;

    public ConnectionTask(BluetoothDevice server, UUID uuid) {
        try {
            btSocket = server.createRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) { /* ... */ }
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            btSocket.connect();
        } catch (IOException connectException) {
            try {
                btSocket.close();
            } catch (IOException closeException) { /* ... */ }
            return null;
        }
        ConnectionManager cm = ConnectionManager.getInstance();
        cm.setChannel(btSocket);
        cm.start();
        return null;
    }
}
