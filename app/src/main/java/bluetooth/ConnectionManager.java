package bluetooth;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectionManager extends Thread{
    private BluetoothSocket btSocket;
    private InputStream btInStream;
    private OutputStream btOutStream;
    private boolean stop;
    private static ConnectionManager instance = null;

    private ConnectionManager() {
        stop = true;
    }

    public static ConnectionManager getInstance() {
        if (instance == null)
            instance = new ConnectionManager();
        return instance;
    }

    public void setChannel(BluetoothSocket socket) {
        btSocket = socket;
        try {
            btInStream = socket.getInputStream();
            btOutStream = socket.getOutputStream();
        } catch (IOException e) { /* ... */ }
        stop = false;
    }

    public void run() {
        byte[] buffer = new byte[1024];
        int nBytes = 0;
        String message = "";
        while (!stop) {
            try {
                nBytes = btInStream.read(buffer);
                for (int i = 0; i < nBytes; i++) {
                    if ((char)buffer[i] == '.') {

                        new MessageHandlerTask().execute(message);

                        Log.e("SD-bluetooth_in", message);

                        message = "";
                    } else {
                        message += (char)buffer[i];
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                stop = true;
            }
        }
    }

    public boolean write(String message) {
        Log.e("SD-bluetooth_out", message);

        message += ".";

        byte[] bytes = message.getBytes();
        if (btOutStream == null) return false;
        try {
            btOutStream.write(bytes);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void cancel() {
        try {
            btSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
