package it.geenhousemobile;

import android.support.v7.app.AppCompatActivity;

import observer.Observable;
import observer.Observer;

public abstract class AbstractActivity extends AppCompatActivity implements Observer {

    /*
     * Adds the observer to every activity that implements this class
     */
    @Override
    protected void onResume() {
        super.onResume();
        Observable.get().addObserver(instance());
    }

    /*
     * Removes the observer to every activity that implements this class
     */
    @Override
    protected void onPause() {
        super.onPause();
        Observable.get().removeObserver(instance());
    }

    /*
     * Prevents the application to jump back to the previous activity
     */
    @Override
    public void onBackPressed() {
        if (true) {
        } else {
            super.onBackPressed();
        }
    }

    /*
     * Gets the events
     */
    @Override
    public void notifyEvent(String message) {

    }

    abstract Observer instance();
}
