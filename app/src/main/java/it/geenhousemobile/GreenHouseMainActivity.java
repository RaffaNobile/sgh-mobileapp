package it.geenhousemobile;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import bluetooth.ConnectionInit;
import bluetooth.ConnectionManager;



public class GreenHouseMainActivity extends AppCompatActivity {

    private final int ENABLE_BT_REQ = 1;
    private String humidityValue;
    private ProgressBar humidityBar;
    private TextView label;
    private RequestQueue mQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.green_house_main);
        humidityBar = findViewById(R.id.humidityBar);
        label = findViewById(R.id.textHumidity);
        mQueue = Volley.newRequestQueue(this);
        try {
            this.enableBluetooth();
            this.ButtonHandler();
        } catch (Exception e){
            e.printStackTrace();
        }
        final Handler ha = new Handler();
            ha.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Main:","Main Run" );
                getHumidity();
                ha.postDelayed(this, 100);
            }
        }, 1000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ConnectionManager.getInstance().cancel();
    }
    public void setHumidity(String humidity){
        this.humidityValue = humidity;
    }

    public void getHumidity() {

        String url = "https://e598299c.ngrok.io/get_humidity";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response.toString());
                            String humidityString = jsonObj.getString("Humidity");
                            setHumidity(humidityString);
                            label.setText("Humidity: " + humidityString + "%");
                            humidityBar.setProgress(Integer.parseInt(humidityString));
                            Log.d("Humidity", humidityString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void ButtonHandler(){

        Button btnStop = findViewById(R.id.btnStop);
        Button btnPmin = findViewById(R.id.btnPmin);
        Button btnPmed = findViewById(R.id.btnPmed);
        Button btnPmax = findViewById(R.id.btnPmax);
        Button btnExit = findViewById(R.id.btnExit);

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManager.getInstance().write("1");
            }
        });

        btnPmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManager.getInstance().write("2");
            }
        });

        btnPmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManager.getInstance().write("3");
            }
        });

        btnPmax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManager.getInstance().write("4");
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManager.getInstance().write("5");
            }
        });
    }

    @Override
    public void onActivityResult(int reqID, int res, Intent data) {
        try {
            if (reqID == ENABLE_BT_REQ && res == Activity.RESULT_OK) {
                new ConnectionInit().init();
            }
            if (reqID == ENABLE_BT_REQ && res == Activity.RESULT_CANCELED) {
                finish();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void enableBluetooth() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            finish();
        } else if (!btAdapter.isEnabled()) {
            startActivityForResult(
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    ENABLE_BT_REQ);
        } else {
            new ConnectionInit().init();
        }
    }
}
