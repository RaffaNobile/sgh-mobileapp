package observer;

public interface Observer {

    void notifyEvent(String message);
}
