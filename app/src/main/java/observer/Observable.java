package observer;

import java.util.HashSet;
import java.util.Set;

public class Observable {
    private static Observable singleton;
    private Set<Observer> observers = new HashSet<>();

    private Observable() {}

    public static Observable get() {
        if (singleton == null) {
            singleton = new Observable();

        }
        return singleton;
    }

    public void addObserver(Observer obs) {
        observers.add(obs);
    }

    public void removeObserver(Observer obs) {
        observers.remove(obs);
    }

    public void notifyAll(String message) {
        for(Observer elem : observers) {
            elem.notifyEvent(message);
        }
    }
}
